---
layout: page
title: About
permalink: /about/
---

Hi, my name is Mateus "N2OMatt" Mesquita! I'm from a small city in Brazil called 
[Santa Rita do Sapucai](https://en.wikipedia.org/wiki/Santa_Rita_do_Sapuca%C3%AD).

I like build things, play chess, listen Jazz and Synthwave and have a nice 
conversation. 

Previously I worked with amazing guys and in very, very cool places like:

- Fire Horse Studios
- Ginga.One
- Imidiar Target Virtual
- Microsoft Innovation Center
- i4Mobi

You can check some of my work on the [Projects](/projects) section.

I also [lectured](/lectures) several times for [Microsoft Innovation Center](https://technet.microsoft.com/pt-br/mt744389) and Microsoft - the last one while I was a [Microsoft Student Partner](https://www.microsoft.com/en-hk/msp).


I'm a passionate guy trying to make the world a better place with my skills.

I create a lot of FREE SOFTWARE with [Amazing Cow](http://github.com/AmazingCow) 
and also [contributed](/projects/#floss-contributions) for some FLOSS projects, 
like [LibreFlix](http://libreflix.org) and [Cocos2d-x](http://www.cocos2d-x.org).


This blog is intended to be a way to help me to **teach**, **learn** and 
**express** my opinions and vision of the world. That say I must reinforce 
that **NONE** of the things that are said/written  here (_necessarily_) represents 
any  official opinions and/or positions of my employers. **All things are my 
responsibility and only mine**.

Finally I'm really glad that you're here! I hope that you enjoy the content 
and any feedback is most welcome!

So let's connect :)

- n2omatt@druid.games  - Email address.
- [n2omatt](https://gitlab.com/n2omatt) - GitLab.
- [n2omatt](https://www.linkedin.com/in/n2omatt/) - LinkedIn.
- [@n2omatt](https://twitter.com/n2omatt) - Twitter.
- [@n2omatt](https://instagram.com/n2omatt) - Instagram.
- [n2omatt](https://libreflix.org/u/n2omatt) - LibreFlix.
- [n2omatt](https://en.gravatar.com/n2omatt) - Gravatar.