---
layout: page
title: Journal
permalink: /journal
---


--------------------------------------------------------------------------------
### Table of Contents:

- [Events](#events)
- [Movies](#movies)
- [Papers](#papers)
- [Books](#books)
    - [General](#general)
    - [Philosophy](#philosophy)
    - [Tech](#tech)


<!-- ----------------------------------------------------------------------- -->
<!-- Events                                                                  -->
<!-- ----------------------------------------------------------------------- -->
--------------------------------------------------------------------------------
## Events

- **Lunch with Steve from Versus Evil and Firehorse team** - Flying Sushi  - (Jan 25, 2017) - <a href="/journal/lunch_with_steve">      (photos)</a>
- **Unity - Confraternização de final de ano**             - Ludos Luderia - (Dec 21, 2016) - <a href="/journal/confraternizacao_unity">(photos)</a>
- **2º Happy Hour in English / CUBO Bilíngue**             - Sede do Cubo  - (Nov 25, 2016) - <a href="/journal/cubo_bilingue">         (photos)</a>
- **Aniversário cubo.network**                             - Sede do Cubo  - (Oct 20, 2016) - <a href="/journal/aniversario_cubo">      (photos)</a>



<!-- ----------------------------------------------------------------------- -->
<!-- Movies                                                                  -->
<!-- ----------------------------------------------------------------------- -->
--------------------------------------------------------------------------------
## Movies

Movies **FREELY** available on **<a href="https://libreflix.org/">LibreFlix</a>**.
- **<a href="https://libreflix.org/assistir/algorithm-o-filme-hacker"> Algorithm</a>** (2015)
- **<a href="https://libreflix.org/assistir/freenet">Freenet</a>** (2016)
- **<a href="https://libreflix.org/assistir/the-code">The Code</a>** (2001)
- **<a href="https://libreflix.org/assistir/the-internets-own-boy"> The Internet's Own Boy: The Story of Aaron Swartz</a>** (2014)
- **<a href="https://libreflix.org/assistir/tpb-afk">TPB AFK</a>** (2013)
- **RiP!: A Remix Manifesto** (2008)


Others...

- **<a href="https://vimeo.com/150866784">Babás</a>** (2010)
- **007 - Die Another Day** (2002)
- **007 - GoldenEye** (1995)
- **007 - The World Is Not Enough** (1999)
- **007 - Tomorrow Never Dies** (1997)
- **A Beautiful Mind** (2001)
- **A Knight's Tale** (2001)
- **Big Hero 6** (2014)
- **Catch Me If You Can** (2002)
- **Cidade de Deus** (2002)
- **DeadPool** (2016)
- **Django Unchained** (2012)
- **Escape to Victory** (1981)
- **EX_MACHINA** (2015)
- **Gamer** (2009)
- **Gladiator** (2000)
- **Goodfellas** (1990)
- **I Am Legend** (2007)
- **I Robot** (2004)
- **I.T.** (2016)
- **Independence Day** (1996)
- **Independence Day** (2016)
- **Indie Game: The Movie** (2012)
- **Inglourious Bastards** (2009)
- **Into the Wild** (2007)
- **King Arthur** (2004)
- **Léon: The Professional** (1994)
- **Lionheart** (1991)
- **Men in Black II** (2002)
- **Men in Black** (1997)
- **Meu Nome Não É Johnny** (2008)
- **Minority Report** (2002)
- **Monsters University** (2013)
- **Okja** (2017)
- **Pride & Prejudice** (2005)
- **Pulp Fiction** (1995)
- **Rise of the Planet of the Apes** (2011)
- **RoboCop 2** (1990)
- **Robocop 3** (1993)
- **Robocop** (1987)
- **RoboCop** (2014)
- **Rush** (2013)
- **Saving Private Ryan** (1998)
- **Signs** (2002)
- **Star Wars Episode I The Phantom Menace** (1999)
- **Star Wars Episode II Attack of the Clones** (2002)
- **Star Wars Episode III Revenge of the Sith** (2005)
- **Star Wars Episode IV A New Hope** (1977)
- **Star Wars Episode V The Empire Strikes Back** (1980)
- **Star Wars Episode VI Return of the Jedi** (1983)
- **Steve Jobs: The Billion Dollar Hippy** (2011)
- **Steve Jobs: The Lost Interview** (2012)
- **The Hobbit An Unexpected Journey** (2012)
- **The Hobbit The Battle of the Five Armies** (2014)
- **The Hobbit The Desolation of Smaug** (2013)
- **The Intern** (2015)
- **The Last Samurai** (2003)
- **The Lord of the Rings The Fellowship of the Ring** (2001)
- **The Lord of the Rings The Return of the King** (2003)
- **The Lord of the Rings The Two Towers** (2002)
- **The Lovely Bones** (2009)
- **The Man Who Knew Infinity** (2015)
- **The Matrix** (1999)
- **The Secret Life of Pets** (2016)
- **The Theory of Everything** (2014)
- **Trainspotting** (1996)
- **Tremors** (1990)
- **Tron Legacy** (2010)
- **Up** (2009)
- **Vanilla Sky** (2001)
- **Warcraft** (2016)
- **WarGames** (1983)
- **Wreck-It Ralph** (2012)

<!-- End of Movies -->


<!-- ----------------------------------------------------------------------- -->
<!-- Papers                                                                  -->
<!-- ----------------------------------------------------------------------- -->
--------------------------------------------------------------------------------
## Papers

-  [Interaction with Groups of Autonomous Characters](http://www.red3d.com/cwr/papers/2000/pip.html)
(Craig Reynolds)

<!-- End of Papers -->


<!-- ----------------------------------------------------------------------- -->
<!-- Books                                                                   -->
<!-- ----------------------------------------------------------------------- -->
--------------------------------------------------------------------------------
## Books

<!-- ----------------------------------------------------------------------- -->
<!-- General -->
#### General 

- **1984**, George Orwell (9788535914849)
- **À espera de um milagre**, Stephen King (9788539000166)
- **A Revolução dos Bichos**, George Orwell (9788535909555)
- **As Aventuras de Pinóquio**, Carlo Collodi (9788540501478)
- **Assasin’s Creed - Irmandade**, Oliver Bowden (9788501095749)
- **Assassin’s Creed - Renascença**, Oliver Bowden (9788501091338)
- **De conto em conto**, Machado,  Azevedo, Andrade, Sabino, Angelo, Barreto, Vilela, Telles, de Assis, Rey, Bandeira, Piroli (97885082711)
- **Dom Casmurro**, Machado de Assis (9788532651686)
- **Evolução**, Brian e Deborah Charlesworth (9788525426598)
- **Iracema**, José de Alencar (9788532651648)
- **Memórias de um Sargento de Milícias**, Manuel Antônio de Almeida (9788532651679)
- **Memórias Póstumas de Brás Cubas**, Machado de Assis (9788577991761)
- **O Alquimista**, Paulo Coelho (9788598559469)
- **O Cortiço**, Aluísio Azevedo (9788500005785)
- **O Homem que sabia demais**, David Leavitt (9788563219688)
- **O Maravilhoso Mágico de OZ**, L. Frank Baum (9788572327633)
- **Orgulho e Preconceito**, Jane Austen (9788525419644)
- **Os Três Mosqueteiros**, Alexandre Dumas (9788700516849)
- **Outsiders**, Susan E. Hinton (978851105040x)
- **Príncipe da Pérsia**, Mchner, Sina, Pham, Puvilland (9788501086388)
- **Pulp**, Bukowski (9788525418630)
- **Steve Jobs em 250 frases**,Alan Ken Thomas (9780576846109)
- **Uma mente brilhante**, Syvia Nasar (978857799045)

<!-- End of Books General -->

<!-- ----------------------------------------------------------------------- -->
<!-- Philosophy -->
#### Philosophy

- **A Arte da Guerra**, Sun Tzu (9788525410594)
- **A República**, Platão (9788520927076)
- **Apologia de Sócrates**, Platão (9788500801190)
- **Arte Poética**, Aristóteles (9788572326103)
- **Da felicidade**, Sêneca (9788525417541)
- **Da tranquilidade da alma**, Sêneca (9788525417541)
- **Da vida retirada**, Sêneca (9788525417541)
- **Discurso do Método**, Descartes (No ISBN)
- **Do Espiríto Geométrico / Pensamentos**, Pascal (8575568353)
- **Édipo Rei Antígona**, Sófocles (9798572324884)
- **O Existencialismo é um Humanismo**, Jean-Paul Sartre (9788532642868)
- **O Príncipe**, Maquiavel (9788525408952)
- **Platão e a Retórica de Filósofos e Sofistas**, Marina McCoy, (9788537006252)
- **Sêneca, O Filósofo Estoico preceptor e assessor de Nero**, Luiz Feracine (9788576359067)
- **Tratado sobre a clemência**,Sêneca(9788532645128)

<!-- End of Books Philosophy -->

<!-- ----------------------------------------------------------------------- -->
<!-- Tech -->
#### Tech

- **1001 Video Games**, Tony Mott / Peter Molyneux (9788575429198)
- **2D Graphics Programming for Games**, John Pile (9781466501904)
- **A arte da tese**, Michael Beaud (9788577993687)
- **A Realidade em Jogo**, Jane McGonigal (9788576845225)
- **Algoritmos Estruturados**, Farrer, Becker, Faria, de Matos, dos Santos, Maia (9788570300565)
- **As Leis Fundamentais do Projeto de Software**, Max Kanat-Alexander (9788575223024)
- **Building Android Games with Cocos2d-x**, Raydelto Hernandez (9781785283833)
- **C++ Manual de Referência Comentado**, Ellis and Stroustrup (8570017863)
- **Construindo Aplicacoes Web com PHP e MySQL**, Andre Milani (9788575222195)
- **GNU Coding Standards**, Richard Stallman.
- **Modelos Clássicos de Computação**, Corrêa da Silva, Vieira de Melo (8522105189)
- **Monte seu Protótipo ISA Controlado por FPGA**, Alexandre Mendonça, Ricardo Zelenovsky (9788587385062)
- **Programação Avançada em Linux**, Gleicon da Silveira Moraes (8575220764)
- **Running Linux**, Matthias Kalle Dalheimer / Matt Welsh (9780596007607)
- **SDL Game Development**, Shaun Mitchell (9781849696821)
- **Sistemas Operacionais**, Eloá Jane Fernandes Mateus (9788576056669)
- **The Cathedral and the Bazaar**, Eric S. Raymond.
- **Turbo C++**, Herbert Schildt (No ISBN)
