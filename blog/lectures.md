---
layout: page
title: Lectures
permalink: /lectures
---

<!-- COWTODO(n2omatt): Add a description.... -->
Lectures that I gave a long time ago. While I don't quite remember 
the exact dates, all of them was around of ~2012 - ~2014 time span. 
Even worse I don't have photos for all of them :`/

They're list in no order but alphabetically.

<!-- ----------------------------------------------------------------------- -->
<!-- App Day -->
- **App Day** @ Puc Minas, Belo Horizonte - MG (As MSP)
(<a href="/lectures/app_day">Photos</a>)


<!-- ----------------------------------------------------------------------- -->
<!-- Cecon Nerd  -->
- **Cencon Nerd** @ Cecon, Belo Horizonte - MG
(<a href="/lectures/cecon_nerd">Photos</a>)


<!-- ----------------------------------------------------------------------- -->
<!-- Nokia Game Developers Day  -->
- **Nokia Game Developers Day** @ Puc Minas, Belo Horizonte - MG (As intern of MicBH)
(<a href="/lectures/nokia_game_developers_day">Photos</a>)


<!-- ----------------------------------------------------------------------- -->
<!-- Semana da Computação -->
- **Semana da Computação** @ Inatel, Santa Rita do Sapucaí - MG (As MSP)


<!-- ----------------------------------------------------------------------- -->
<!-- Windows 8 Camp -->
- **Windows 8 Camp** @ Puc Minas, Belo Horizonte - MG (As MSP)
(<a href="/lectures/windows_8_camp">Photos</a>, <a href="https://win8campbh.codeplex.com/">Codeplex</a>)

<!-- ----------------------------------------------------------------------- -->
<!-- Windows Developer Day  -->
- **Windows Developer Day** @ Unifei, Itajubá - MG (As MSP)
(<a href="/lectures/windows_developer_day">Photos</a>)

<!-- ----------------------------------------------------------------------- -->
<!-- Windows Phone Camp -->
- **Windows Phone Camp** @ Puc Minas, Belo Horizonte - MG (As intern of MicBH)
