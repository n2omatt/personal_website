---
layout: page
title: Projects
permalink: /projects/
---


--------------------------------------------------------------------------------
### Table of Contents:
- [Current Projects](#current-projects)
    - [druid.games](#druidgames)
- [Old Projects](#old-projects)
    - [Amazing Cow](#amazing-cow)
    - [Stoic Studio](#stoic-studio)
    - [Firehorse](#firehorse)
    - [Other games published by Versus Evil](#other-games-published-by-versus-evil)
    - [Ginga.One](#gingaone)
    - [Imidiar](#imidiar)
    - [Eazz](#eazz)
    - [Devilware](#devilware)
- [Floss Contributions](#floss-contributions)

<!-- /TOC -->


<!-- ----------------------------------------------------------------------- -->
<!-- Current Projects                                                        -->
<!-- ----------------------------------------------------------------------- -->
--------------------------------------------------------------------------------
## Current Projects


This is the mainly projects that I'm currently working on.

### [druid.games](http://druid.games)



<br>

<!-- ----------------------------------------------------------------------- -->
<!-- Old Projects                                                            -->
<!-- ----------------------------------------------------------------------- -->
--------------------------------------------------------------------------------
## Old Projects

Projects that I was involved in earlier point in time...

<!-- ----------------------------------------------------------------------- -->
<!-- Amazing Cow's                                                           -->
<!-- ----------------------------------------------------------------------- -->
### Amazing Cow


<!-- ----------------------------------------------------------------------- -->
<!-- Memory -->
<!-- COWTODO(n2omatt): Add webstuff review.... -->
<a href="https://play.google.com/store/apps/details?id=com.amazingcow.gamememory&hl=en_US">
    <img src="/img/projects/memory.png">
    <b>Memory - Amazing Cow</b>
</a>
(<a href="https://play.google.com/store/apps/details?id=com.amazingcow.gamememory&hl=en_US">Google Play</a>) - Programmer / Artist / Sound


<!-- ----------------------------------------------------------------------- -->
<!-- Cosmic Intruders -->
<!-- COWTODO(n2omatt): Add github review.... -->
<a href="https://amazingcow.itch.io/cosmic">
    <img src="/img/projects/cosmic_intruders.png">
    <b>Cosmic - Amazing Cow</b>
</a> - Programmer

<!-- ----------------------------------------------------------------------- -->
<!-- Bow and Arrow -->
<!-- COWTODO(n2omatt): Add github review.... -->
<a href="https://amazingcow.itch.io/bow-and-arrow">
    <img src="/img/projects/bow_and_arrow.png">
    <b>Bow & Arrow - Amazing Cow</b>
</a> - Programmer

<!-- ----------------------------------------------------------------------- -->
<!-- Kaboom -->
<!-- COWTODO(n2omatt): Add github review.... -->
<a href="https://amazingcow.itch.io/kaboom">
    <img src="/img/projects/kaboom.png">
    <b>Kaboom - Amazing Cow</b>
</a> - Programmer


<!-- ----------------------------------------------------------------------- -->
<!-- FLOSS Projects -->
 <!-- Games --> 
<a href="https://github.com/AmazingCow-Game">
    <img src="/img/projects/ac_game.png">
    <b>Amazing Cow's Games</b>
</a> (<a href="https://github.com/AmazingCow-Game">Github</a>) - Programmer 

 <!-- Game Core --> 
<a href="https://github.com/AmazingCow-Game-Core">
    <img src="/img/projects/ac_game_core.png">
    <b>Amazing Cow's Games Cores</b>
</a> (<a href="https://github.com/AmazingCow-Game-Core">Github</a>) - Programmer 

 <!-- Game Framework --> 
<a href="https://github.com/AmazingCow-Game-Framework">
    <img src="/img/projects/ac_game_framework.png">
    <b>Amazing Cow's Game Frameworks</b>
</a> (<a href="https://github.com/AmazingCow-Game-Framework">Github</a>) - Programmer 

 <!-- Game Tools --> 
<a href="https://github.com/AmazingCow-Game-Tool">
    <img src="/img/projects/ac_game_tool.png">
    <b>Amazing Cow's Game Tools</b>
</a> (<a href="https://github.com/AmazingCow-Game-Tool">Github</a>) - Programmer 

 <!-- Libs --> 
<a href="https://github.com/AmazingCow-Libs">
    <img src="/img/projects/ac_libs.png">
    <b>Amazing Cow's Libs</b>
</a> (<a href="https://github.com/AmazingCow-Libs">Github</a>) - Programmer 

 <!-- Tools --> 
<a href="https://github.com/AmazingCow-Tools">
    <img src="/img/projects/ac_tools.png">
    <b>Amazing Cow's Tools</b>
</a> (<a href="https://github.com/AmazingCow-Tools">Github</a>) - Programmer 


<br>
<!-- End of Amazing Cow -->


<!-- ----------------------------------------------------------------------- -->
<!-- Stoic Studio                                                            -->
<!-- ----------------------------------------------------------------------- -->
### Stoic Studio

<!-- ----------------------------------------------------------------------- -->
<!-- TBS1 -->
<a href="http://store.steampowered.com/app/237990/The_Banner_Saga">
    <img src="/img/projects/banner_saga_1.jpg">
    <b>The Banner Saga 1</b>
</a> - Additional Programming

<!-- ----------------------------------------------------------------------- -->
<!-- TBS2 -->
<a href="http://store.steampowered.com/app/281640/The_Banner_Saga_2/">
    <img src="/img/projects/banner_saga_2.jpg"/>
    <b>The Banner Saga 2</b>
</a> - Additional Programming

<!-- ----------------------------------------------------------------------- -->
<!-- Killers and Thieves -->
<a href="http://store.steampowered.com/app/382330/Killers_and_Thieves/">
    <img src="/img/projects/killers_and_thieves.jpg"/>
    <b>Killers and Thieves</b>
</a> - Programming

<br>
<!-- End Stoic Studio -->


<!-- ----------------------------------------------------------------------- -->
<!-- Firehorse                                                               -->
<!-- ----------------------------------------------------------------------- -->
### Firehorse

<!-- ----------------------------------------------------------------------- -->
<!-- Like a Boss -->
<a href="https://play.google.com/store/apps/details?id=com.versusevil.likeaboss&hl=en">
    <img src="/img/projects/like_a_boss.png"/>
    <b>Like a Boss</b>
</a> - Additional Programming

<!-- ----------------------------------------------------------------------- -->
<!-- Fist Punch 2 -->
<a href="http://www.cartoonnetworkasia.com/game/regular-show-fist-punch2">
    <img src="/img/projects/fist_punch.jpg">
    <b>Fist Punch 2 - Cartoon Network</b>
</a> - Additional Programming

<br>
<!-- End Firehorse -->


<!-- ----------------------------------------------------------------------- -->
<!-- Maybe versus evil games???                                              -->
<!-- ----------------------------------------------------------------------- -->
### Other games published by Versus Evil

<!-- ----------------------------------------------------------------------- -->
<!-- Let them Come -->
<a href="http://store.steampowered.com/app/505630/Let_Them_Come/">
    <img src="/img/projects/let_them_come.jpg"/>
    <b>Let them Come - Klemen Lozar</b>
</a> - AlienFx's SDK integration with Game Maker.

<br>
<!-- End Maybe versus evil games??? -->


<!-- ----------------------------------------------------------------------- -->
<!-- Ginga.One                                                               -->
<!-- ----------------------------------------------------------------------- -->
### Ginga.One

<!-- ----------------------------------------------------------------------- -->
<!-- Pequenos Criativos -->
<a href="http://www.pequenoscriativos.com.br">
    <img src="/img/projects/pequenos_criativos.png"/>
    <b>Pequenos Criativos</b>
</a> - iOS Programming.

<!-- ----------------------------------------------------------------------- -->
<!-- Porto Seguro Auto -->
<a href="https://itunes.apple.com/br/app/porto-seguro-auto-auto-socorro/id940308962?mt=8">
    <img src="/img/projects/porto_seguro_auto.jpg"/>
    <b>Porto Seguro Auto</b>
</a> - iOS Programming.

<br>
<!-- End Ginga.One -->


<!-- ----------------------------------------------------------------------- -->
<!-- Imidiar                                                                 -->
<!-- ----------------------------------------------------------------------- -->
### Imidiar

<!-- ----------------------------------------------------------------------- -->
<!-- Photo Totem -->
<a href="https://www.facebook.com/imidiar">
    <img src="/img/projects/imidiar.jpg"/>
    <b>Imidiar's Photo Totem</b>
</a> --
(<a href="https://github.com/AmazingCow-Imidiar/PhotoTotem">Github</a>) - Programming.

<br>
<!-- End Imidiar -->


<!-- ----------------------------------------------------------------------- -->
<!-- Eazz                                                                    -->
<!-- ----------------------------------------------------------------------- -->
### Eazz

<!-- ----------------------------------------------------------------------- -->
<!-- Donkey.bas -->
<img src="/img/projects/donkey_bas.png"/>
    <b>Donkey.bas</b>  (Windows Phone) - Programmer / Artist.
    <br>
    This game cannot be found on store anymore... but here's
    the <a href="./archive/projects/DonkeyBas.html">archive</a>
    describing it.

<!-- ----------------------------------------------------------------------- -->
<!-- MetroGenius -->
<img src="/img/projects/metro_genius.png"/>
    <b>MetroGenius</b>  (Windows Phone)
    - Programmer / Artist.
    <br>
    This game cannot be found on store anymore... but here's
    the <a href="./archive/projects/MetroGenius.html">archive</a>
    describing it - and a <a href="./archive/projects/MetroGeniusReview.html">
    review</a>.

<br>
<!-- End Eazz -->


<!-- ----------------------------------------------------------------------- -->
<!-- Devilware                                                               -->
<!-- ----------------------------------------------------------------------- -->
### Devilware

**Pirate Treasure** -- Programmer / Artist.

**Tic Tac Toe** -- Programmer / Artist.

<br>
<!-- End Devilware -->


<!-- ----------------------------------------------------------------------- -->
<!-- Open Source Contribs                                                    -->
<!-- ----------------------------------------------------------------------- -->
--------------------------------------------------------------------------------
## Floss Contributions

<!-- ----------------------------------------------------------------------- -->
<!-- Libreflix  -->
<b><a href="http://www.libreflix.org">libreflix</a></b>
( 
    <a href="https://notabug.org/libreflix/libreflix/pulls/17">#17</a>
)

<!-- ----------------------------------------------------------------------- -->
<!-- Cocos2dx -->
<b><a href="http://www.cocos2d-x.org/">Cocos2d-x</a></b>
(
    <a href="https://github.com/cocos2d/cocos2d-x/pull/16466">#16466</a>,
    <a href="https://github.com/cocos2d/cocos2d-x/pull/16789">#16789</a>
)

<!-- ----------------------------------------------------------------------- -->
<!-- 2048py -->
<b><a href="https://github.com/davidsousarj/2048py">2048py</a></b>
(
    <a href="https://github.com/davidsousarj/2048py/pull/2">#2</a>
)

<!-- End Open Source Contribs -->
